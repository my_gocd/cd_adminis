# GOCD SETUP AND CONFIGURATION

This repository consists of multiple ansible playbooks and roles for CD Admin configuration. CD admin is the pipeline group which conntains Administration pipelines.

## cd_administration

Ansible-roles and playbooks for configuration of:

- config-repo
- pipeline-groups
- gocd-role management

## Overview

Playbooks for managing GoCD configuration: config repos, environments, pipeline groups, roles...

## Agents

This playbook assigns all available agents to all the already created environments.
Usage:

```bash
    ansible-playbook -i inventories/development manage_gocd_agents.yml -e "GOCD_TOKEN=<gocd_token>"
```

## Config repos

Usage:

```bash
    ansible-playbook -i inventories/development manage_gocd_config_repo.yml --ask-vault-pass -e PROJECT_CODE=18023-BLP
```

## Environments

Use the Ansible vault to store the secure environment variables.
This playbook will decrypt the variables from the Ansible vault, encrypt them again in the target GoCD environment and assign them to the final environment.
Usage:

```bash
    ansible-playbook -i inventories/development manage_gocd_environment.yml --ask-vault-pass -e "isProjectOnboarding=true PROJECT_CODE=18023-BLP GOCD_TOKEN=<gocd_token> GOCD_USER=<gocd_user> GOCD_PASSWORD=<gocd_pass>"

    ansible-playbook -i inventories/development manage_gocd_environment.yml --ask-vault-pass -e "isProjectOnboarding=false GOCD_TOKEN=<gocd_token> GOCD_USER=<gocd_user> GOCD_PASSWORD=<gocd_pass>"
```

## Pipeline groups

TODO
Usage:

```bash
    ansible-playbook -i inventories/development manage_gocd_pipeline_group.yml --ask-vault-pass -e PROJECT_CODE=18023-BLP
```

## Roles

TODO
Usage:

```bash
    ansible-playbook -i inventories/development manage_gocd_roles.yml --ask-vault-pass -e PROJECT_CODE=18023-BLP
```

## Templates

TODO
Usage:

```bash
    ansible-playbook -i inventories/development manage_template.yml --ask-vault-pass -e "GOCD_TOKEN=<gocd_TOKEN>"
```

## Update Docker Images in pipelines

This pipeline updates the latest images in all Gocd agents using 'docker pull' command. It verifies the Docker repositories are accessible execuring the 'docker login' command as a part of the pipeline.

The playbook requires two extra variables:

- GO_SERVER_URL: this parameter refers to the environment from where we want to extract the list of agents we want to connect.
- GOCD_TOKEN: this parameter refers to the Gocd token recently added in the 19.2.0 version (no user and password required).
- ANSIBLE_USER: this parameter refers to the SSH user required to connect to the servers.
- ANSIBLE_PASS: this parameter refers to the SSH password required to connect to the servers.

Dependencies:
Ansible user and password should be provided as environment variables: 'ANSIBLE_USER' and 'ANSIBLE_PASS'.

Usage:

```bash
    ansible-playbook manage_gocd_docker_images.yml -e "GO_SERVER_URL=${GO_SERVER_URL} GOCD_TOKEN=${GOCD_TOKEN} ANSIBLE_USER=${ANSIBLE_USER} ANSIBLE_PASS=${ANSIBLE_PASS} " --ask-vault-pass
```

## Manage Docker Image Versions in pipelines

This pipeline manages the Docker image versions in all Gocd agents using 'docker' commands. It verifies the Docker image versions exist (if not, they are created) and add the new version to the corresponding environment.

The playbook requires two extra variables:

- GO_SERVER_URL: this parameter refers to the environment from where we want to extract the list of agents we want to connect.
- GOCD_TOKEN: this parameter refers to the Gocd token recently added in the 19.2.0 version (no user and password required).
- ANSIBLE_USER: this parameter refers to the SSH user required to connect to the servers.
- ANSIBLE_PASS: this parameter refers to the SSH password required to connect to the servers.

Dependencies:
Ansible user and password should be provided as environment variables: 'ANSIBLE_USER' and 'ANSIBLE_PASS'.

Usage:

```bash
    ansible-playbook playbook_mgmt/manage_gocd_docker_image_versions.yml -e "GO_SERVER_URL=${GO_SERVER_URL} GOCD_TOKEN=${GOCD_TOKEN}" --ask-vault-pass

    or

    ansible-playbook -i inventories/development manage_gocd_docker_image_versions.yml --ask-vault-pass -e "GOCD_TOKEN=${GOCD_TOKEN}"
```
