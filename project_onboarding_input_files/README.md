
# FORM FILE

Before proceeding, it would be nice to understand the concept of config-repo in GoCD.
https://docs.gocd.org/current/advanced_usage/pipelines_as_code.html

## How to write a Form File

* #### Project_Code

      Allianz Project Code, for eg: 18023-BLP for e-Commerce project.

* #### Environment_Name

      This can be `PROD` or `NON-PROD`, depending on the environment for which the pipeline codes are being generated. If the environment is `NON-PROD`, like in DEV/SIT/UAT, the pipeline codes are expected to be in the `non-prod-config-repo` repository.

* #### hipchatRoomId
      Find the hipchat room id from your hiphat room (only room admins can see this )

* #### hipchatToken
      Find the hipchat token from your hiphat room (only room admins can see this )

* ### Modules
      This the section where you list the modules /services which needs to generate is the list of modules which you need to generate pipeline codes. For eg: if you have 10 microservices to be build and deployed, specify the details of all services in this section.

* #### Module_Name
      Unique name of the module. This is for name the build pipeline. Need not to be same as of artifact id.

* #### materialURL
      Git repository location of the service.

* #### Branch
      Branch of the source code repository. It can be either master or release. If the branch is release, the build pipeline name will be Build_release_<Module_Name> and if the branch is master, the build pipeline name will be Build_master_<Module_Name>

* #### Artifact_Id
      Artifact Id of the module. Same as that in the pom.xml

* #### Artifact_Type
      Artifact type of the module. It can be Release / Snapshot

Fill all these details, commit to your branch and create a merge request in GoCD repository, some one from devops team will review it and merge the code to master.