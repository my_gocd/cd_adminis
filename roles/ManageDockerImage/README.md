# Ansible Role: ManageDockerImage

Creates and/or updates Docker images in the agent servers and set the Docker image versions as an environment variable in the specified environments.

## Requirements

GoCD must be installed first.

## Role Variables

Available variables are listed below, along with default values:

```bash
    GO_USER: go
    DOCKER_REG_USERNAME: "{{ AZID_AD_USER }}"
    DOCKER_REG_PASSWORD: "{{ AZID_AD_PASSWORD }}"
    DOCKER_REGISTRIES:
        - "private.docker.registry.azlife.allianz.co.id"
        - "public.docker.registry.azlife.allianz.co.id"
    DOCKER_PRIVATE: "private."
    DOCKER_PUBLIC: "public."
    DOCKER_IMAGES_PREFIX: "docker.registry.azlife.allianz.co.id/allianz/"
    DOCKER_IMAGES_SUFIX: ":latest"
```

## Dependencies

None.

## Example Playbook

```bash
- hosts: dynamic_agents
  gather_facts: no

  vars:
    yaml_file: project_onboarding_input_files/docker_images.yml
  tasks:
    - name: 
      include_role:
        name: ManageDockerImage
      vars:
        environment_key: "{{ item.key }}"
      loop: "{{ lookup('file', '{{ yaml_file }}' ) | from_yaml }}"
```

## License

Licensed under the Apache License V2.0. See the [LICENSE file](LICENSE) for details.

## Author Information

Allianz Indonesia @ 2019.
