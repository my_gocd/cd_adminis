# Role Name

Manage GoCD repo configuratio through api for various server admin level operations mentioned as in [LCDP-19](https://intranet.allianz.co.id/jira/browse/LCDP-19).

The role only create the repo configuration as it is specified in the configuration file located in [GitLab](https://git.allianz.co.id/git/CDP/Gocd).
NOTE: The role only 'creates' and 'updates' repo configuration, deletion of existing repo configuration is managed separately.

## Requirements

The role makes use of the `csv_to_facts` module for reading the configuration file and store that information in the ansible facts.

## Role Variables

TODO: A description of the settable variables for this role should go here, including
any variables that are in defaults/main.yml, vars/main.yml, and any variables
that can/should be set via parameters to the role. Any variables that are read
from other roles and/or the global scope (ie. hostvars, group vars, etc.) should
be mentioned here as well.

## Dependencies

No dependencies with other roles.

## Example Playbook

TODO: Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: azid.gocd_repo_config, x: 42 }

## License

Licensed under the Apache License V2.0. See the [LICENSE file](LICENSE) for details.

## Author Information

Allianz Indonesia @ 2017.
