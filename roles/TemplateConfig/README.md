# Ansible Role: TemplateConfig

Creates and/or updates GoCD template (https://docs.gocd.org/current/configuration/pipeline_templates.html) on a specific installed GoCD server.

## Requirements

GoCD must be installed first.
Please refer to the playbooks/playbook.yml for installing GoCD environment.

## Role Variables

Available variables are listed below, along with default values:

GOCD_API_TEMPLATE_URL: "{{ GOCD_SERVER_URL }}/templates"

Below variables will be populated on-the-fly:

etag: xxx
template_name: xxx
template_request_body: xxx

## Dependencies

None.

## Example Playbook (using default package, usually OpenJDK 7)

- hosts: servers
  roles:
  - role:  TemplateConfig
    vars:
        template_request_body: "{{ item | to_json }}"
        template_name: "{{item.name}}"

## License

Licensed under the Apache License V2.0. See the [LICENSE file](LICENSE) for details.

## Author Information

Allianz Indonesia @ 2019.
