# Role Name

Manage GoCD pipeline group config through api for various server admin level operations mentioned as in [LCDP-19](https://intranet.allianz.co.id/jira/browse/LCDP-19).

The role only create the pipeline group configuration as it is specified in the configuration file located in [GitLab](https://git.allianz.co.id/git/CDP/Gocd).
NOTE: The role only 'creates' and 'updates' pipeline group configuration, deletion of existing pipeline group configuration is managed separately.

## Author Information

Allianz Indonesia @ 2018.
